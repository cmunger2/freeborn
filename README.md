# README #

#####
Sends Freeborn Data to CCS/CMAP to be processed.


## To Install ##:
1. scp deb file to the remote appliance
2. dpkg -i freeborn_1.1.deb
3. *Optionally* verify scripts are in place

```
#!bash

updatedb
locate -i freeborn
```

Might see something similar:

**locate -i freeborn**

```
#!bash

/etc/cron.d/root_sendFreebornData
/home/sampson/freeborn_1.1.deb
/usr/share/doc/freeborn
/usr/share/doc/freeborn/changelog.Debian.gz
/var/lib/dpkg/info/freeborn.list
/var/lib/dpkg/info/freeborn.md5sums
/var/www/CaseSentry/bin/sendFreeBornData.pl
```

You are looking for specifically:

```
#!bash

/etc/cron.d/root_sendFreebornData
/var/www/CaseSentry/bin/sendFreeBornData.pl
```
### You are done.  Update the tracker. ###
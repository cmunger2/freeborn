#!/usr/bin/env perl
#Script will send freeborn data via email to either CCS/CMAP
#v1.1-8
#Author: Clayton Munger
use warnings;
use strict;
use Data::Dumper;
use lib '/var/www/CaseSentry/lib/Perl';
use ConnectVars;
use ConfigTable;
use DBI qw(:sql_types);
use CS_Diagnostics qw( isActive );

#Globals
my $message;
my $users;
my @emails;
my $sgEmail = 'cs-mailchk@shoregroup.com';
my $crosEmail = 'ros-mailchk@external.cisco.com';
my $hostIssueEmail;
my $fromEmail;
my $hostname = `hostname`;
my $domain = `hostname --domain`;
	chomp($hostname);
	chomp($domain);
my $debug = '0';

#Grab the month before as our script runs on the first of the month
my $date = `date +'%Y%m' -d 'last month'`;
my $year = `date +'%Y'`;
my $mon = `date +'%m' -d 'last month'`;
my $days = LastDayOfMonth($year,$mon);

#make log directory
system("mkdir -p /var/log/freeborn");

#Get connection
getConnectVars or die "Cannot get values ConnectVars\n";
my $hDb = getConnection( 'Main' );

# check if system is active
my $is_active = 1;
if (!isActive($hDb))
{
        my $is_active = 0;
        print "Running on a non-active system.\n";
        exit;
}

#check product type
my $querytext = "SELECT `value` FROM `CaseSentryConfig` WHERE `parm` = 'PRODUCT_TYPE'";
my $query1 = $hDb->prepare($querytext);
$query1->execute();
my @row = $query1->fetchrow_array();
if ($row[0] eq "CROS") {
    push(@emails, $crosEmail);
    $fromEmail =  $hostname . "@" . $domain; 
    $hostIssueEmail = 'clmunger@cisco.com';
    print "\nUsing CROS, $row[0]\n" if $debug;
} else {
    push(@emails, $sgEmail);
    $fromEmail =  $hostname . "@" . $domain;
    $hostIssueEmail = 'clayton.munger@optanix.com';
    print "\nUsing SG, $row[0]\n" if $debug;
}

my $qDate = `date +"%Y-%m"`;
chomp($qDate);

my $query2 = "
	SELECT
	(SELECT value FROM CaseSentryConfig WHERE parm IN('SiteId')) AS 'Site',
	SUBSTRING_INDEX((SELECT `year_month` FROM active_phone_count_report WHERE `year_month` NOT LIKE \'%$qDate%\' GROUP BY `year_month` ORDER BY `year_month` DESC LIMIT 1),'-',2) AS 'Month',
	(SELECT COUNT(*) FROM active_phone_count_report WHERE `year_month` NOT LIKE \'%$qDate%\' GROUP BY `year_month` ORDER BY `year_month` DESC LIMIT 1) AS 'mCount',
	(SELECT COUNT(DISTINCT co.entity) FROM commit_object co JOIN pollers p ON co.poller_group_id = p.group_id WHERE p.enabled = 1) AS 'pCount'
	";

my $sth = $hDb->prepare($query2);
$sth-> execute() or die "Cannot execute query: $sth->errstr\n";
my($siteName, $month, $mCount, $pCount) = $sth->fetchrow_array();
if (!$month) {
    $month = $date;
}
if (!$mCount) {
    $mCount = '0';
}

#Remove endlines from Variables:
chomp($siteName);
chomp($month);
chomp($mCount);
chomp($pCount);

#Catch if any of the appliances don't have the sitename set (This will allow me to correct the issue)
if (lc($siteName) eq 'local'|| $siteName eq ''){
		open F1, '>>', '/var/log/freeborn/freeborn.log'
    		or die "could not open file: $!";
		print F1 "Issue With No Name | $siteName;";
		my $siteInfo = `head -10 /etc/hosts`;
		print F1 $siteInfo;
		my $ms = "/usr/bin/mail -s \'Freeborn Script Issue\' $hostIssueEmail < /var/log/freeborn/freeborn.log";
		`$ms`; 
		print $ms . "\n" if $debug;
		print "Bad SiteName, $siteName\n" if $debug;
		close F1;
		exit;
}

#Trash the dash (month) and Setup Message as CSV
my $mo = $month;
$month =~ s/-//g; 

#Add Up pCount for month:
$pCount = $pCount * 12 * 24 * $days;

$message = "fbStart" . $siteName . "," . $month . "," . $mCount . "," . $pCount . "fbEnd";

#join the emails if multiple
foreach (@emails){
	$users = join(',', @emails);
}

#Email
my $to = $users;
my $from = $fromEmail;
my $subject = 'Freeborn Data|' . $siteName . '|' . $mo;
my $sendEmailCommand = "/bin/echo '" . $message . "' | /usr/bin/mail -s 'Freeborn Data|" . $siteName . "|" . $mo . "' '" . $users . "' -- -f '" . $from . "'";

open F1, '>>', '/var/log/freeborn/freeborn.log'
    or die "could not open file: $!";

print F1 "\nDate: $date\n";
print F1 $sendEmailCommand . "\n";
close F1;
print "Script Completed\n" if $debug;

#Sending Email
`$sendEmailCommand`;

sub LastDayOfMonth {
my($y, $m) = @_;
my $d =(31,28,31,30,31,30,31,31,30,31,30,31)[$m-1];
if ( $m == 2 && (
	($y % 4 == 0) &&
	($y % 100 != 0) &&
	($y % 400 == 0) ) ) {
	$d++;
}
	return $d;
}
